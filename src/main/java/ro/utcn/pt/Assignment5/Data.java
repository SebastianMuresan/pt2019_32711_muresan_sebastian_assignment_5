package ro.utcn.pt.Assignment5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Data {

    public String startDate;
    public String startHour;
    public String finishDate;
    public String finishHour;

    public String getActivity() {
        return activity;
    }

    public String activity;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm:ss");

    public Data(String startDate, String startHour, String finishDate, String finishHour, String activity) {
        this.startDate = startDate;
        this.startHour = startHour;
        this.finishDate = finishDate;
        this.finishHour = finishHour;
        this.activity = activity;
    }

    public Date getStartDate(){
        Date statDate = new Date();

        try {
            statDate = dateFormat.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return statDate;
    }

    public Date getFinishDate(){
        Date finiDate = new Date();

        try {
            finiDate = dateFormat.parse(finishDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finiDate;
    }

    public Long getStartHour(){

        long statHour = 0;
        try {
            statHour = hourFormat.parse(startHour).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return statHour;
    }

    public Long getFinishHour(){
        long finiHour = 0;

        try {
            finiHour = hourFormat.parse(finishHour).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finiHour;
    }

    public Long timeOnActivityMILISECONDS(){
        return getFinishHour() - getStartHour();
    }
}
