package ro.utcn.pt.Assignment5;

import sun.awt.image.IntegerComponentRaster;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * This class manipulates all the data
 */
public class Logic {



    /**
     * This method returns all the information about the activity
     * @return and arrayList containing all The information about the activity
     */
    public ArrayList<Data> getData1(){
        ReadFile readFile = new ReadFile();
        ArrayList<Data> monitoredData = readFile.readFileForData("C:\\Learning\\Java\\TehniciProgramare\\PT2019_32711_Muresan_Sebastian_Assignment5\\Activities.txt");
        return  monitoredData;
    }






    /**
     * This method returns how many days there are in the log
     * @param data
     * @return the number of days
     */
    public long getMonitorDays(ArrayList<Data> data){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        return  data.stream().map(d -> d.startDate).distinct().count();

    }



    /**
     * This Method shows how many times an activity takes place
     * @param data
     * @return A hash map with the name of the activity with and integer showing how many times the activity took place
     */
   public Map<String, Long> activities(ArrayList<Data> data){
        HashMap<String, Long> activities = new HashMap<>();

//        data.stream().forEach((Data element) ->{
//            if(activities.containsKey(element.activity)){
//                long i = activities.get(element.activity);
//                activities.put(element.activity, ++i);
//            }else
//                activities.put(element.activity, (long)1);
//        });

        return data.stream().collect(Collectors.groupingBy(Data::getActivity, Collectors.counting()));
   }

    /**
     * This method shows how many times does an activity take place in one day
     * @param data
     * @return A hash map for every day showing the name of the activity and the nr of how many times it took place in that day
     */


    public Map<Date, Map<String, Long>> numberOfTimesActivityByDay(ArrayList<Data> data){


        return data.stream().collect(Collectors.groupingBy(Data::getFinishDate, Collectors.groupingBy(Data::getActivity, Collectors.counting())));
    }
    /**
     * This method shows how long is an activity
     * @param data
     * @return A hash map for every activity showing how much time does it take
     */
   public Map<String, Long> timeOnActivity(ArrayList<Data> data){
       HashMap<String, Long> timeForActivities = new HashMap<>();

       data.stream().forEach((Data mData) ->{

               long end = mData.getFinishHour();
               long start = mData.getStartHour();

               long duration = end - start;

               timeForActivities.put(mData.activity, duration);

       });
       return timeForActivities;
   }

    /**
     * This method shows how much time, in total does an activity take if you sum up the time
     * @param data
     * @return Hash map with the activity and the full time (all days summed up)
     */
   public Map<String, Long> activityFullTime(ArrayList<Data> data){
        return data.stream()
                .collect(Collectors.groupingBy(Data::getActivity, Collectors.summingLong(Data::timeOnActivityMILISECONDS)));
   }


    public List<String> filterActivitiesWithLessThen5Minutes(ArrayList<Data> data)
    {
        Map<String, Long> activities = data.stream().collect(Collectors.groupingBy(Data::getActivity, Collectors.counting()));

        return data.stream()
                .filter(x -> x.timeOnActivityMILISECONDS() > 30000)
                .map(x -> x.getActivity())
                .collect(Collectors.toList());
    }





}
