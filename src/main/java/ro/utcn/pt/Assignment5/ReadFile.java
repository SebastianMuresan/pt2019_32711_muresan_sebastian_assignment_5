package ro.utcn.pt.Assignment5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

public class ReadFile {

    public ArrayList<MonitoredData> readFile(String filename){
        ArrayList<MonitoredData> monitoredData = new ArrayList<>();

        try(Stream<String> stream = Files.lines(Paths.get(filename))) {
            stream.forEach((String line) -> {

                String[] data = line.split("\t\t");

                MonitoredData readData = new MonitoredData(data[0].trim(), data[1].trim(), data[2].trim());
                monitoredData.add(readData);

            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return monitoredData;
    }

    public ArrayList<Data> readFileForData(String filename){
        ArrayList<Data> monitoredData = new ArrayList<>();

        try(Stream<String> stream = Files.lines(Paths.get(filename))) {
            stream.forEach((String line) -> {

                String[] data = line.split("\t\t");

                MonitoredData readData = new MonitoredData(data[0].trim(), data[1].trim(), data[2].trim());

                Data data1 = new Data(readData.startTime.split(" ")[0], readData.startTime.split(" ")[1],
                        readData.endTime.split(" ")[0], readData.endTime.split(" ")[1], readData.activity);

                monitoredData.add(data1);

            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return monitoredData;
    }
}
