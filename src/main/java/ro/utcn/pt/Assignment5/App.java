package ro.utcn.pt.Assignment5;

import sun.rmi.runtime.Log;

import java.security.KeyPair;
import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Logic logic = new Logic();

        //Monitored Days
        System.out.println(logic.getMonitorDays(logic.getData1())+" Monitored Days");

        //How many times is an activity mentioned
        Map<String, Long> activities = logic.activities(logic.getData1());
        for(Map.Entry<String, Long> elem : activities.entrySet()){
            System.out.println(elem.getKey() + " -> " + elem.getValue());
        }

        //Count how many times has appeared each activity for each day over the monitoring period
        Map<Date, Map<String, Long>> byDay = logic.numberOfTimesActivityByDay(logic.getData1());
        for(Map.Entry<Date, Map<String, Long>> elem : byDay.entrySet()){
            for(Map.Entry<String, Long> act : elem.getValue().entrySet()){
                System.out.println(elem.getKey()+ " ----> " + act.getKey() + " -> " + act.getValue());
            }
        }
        //For each line from the file map for the activity label the duration recorded on that line
        //(end_time-start_time)
        Map<String, Long> timeOfActivities = logic.timeOnActivity(logic.getData1());
        for(Map.Entry<String, Long> time1 : timeOfActivities.entrySet()){
            System.out.println(time1.getKey() + " --- " + time1.getValue() + " miliseconds ---");
        }
//
//        //For each activity compute the entire duration over the monitoring period
        Map<String, Long> fullTime = logic.activityFullTime(logic.getData1());
        for (Map.Entry<String, Long> time : fullTime.entrySet()){
            System.out.println(time.getKey() + " F-- " + time.getValue() + " --F");

        }

        //Filter the activities that have 90% of the monitoring records with duration less than 5 minutes
        List<String> percent = logic.filterActivitiesWithLessThen5Minutes(logic.getData1());

        for(String p : percent){
            System.out.println(p + " < 5 minutes");
        }

    }
}
