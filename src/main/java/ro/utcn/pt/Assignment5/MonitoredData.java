package ro.utcn.pt.Assignment5;

public class MonitoredData {
    String startTime;
    String endTime;
    String activity;

    public MonitoredData(String startTime, String endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }
}
